﻿namespace Rover.TechTest
{
    public interface IInstruction
    {
        void Execute();
    }
}