﻿namespace Rover.TechTest
{
    public class RotateRight : IInstruction
    {
        IRover roverFIELD;

        public RotateRight(IRover roverRobot)
        {
            roverFIELD = roverRobot;
        }

        public void Execute()
        {
            switch (roverFIELD.RoverPosition.Direction)
            {
                case enumDirection.North:
                    roverFIELD.RoverPosition.Direction = enumDirection.East;
                    break;
                case enumDirection.East:
                    roverFIELD.RoverPosition.Direction = enumDirection.South;
                    break;
                case enumDirection.South:
                    roverFIELD.RoverPosition.Direction = enumDirection.West;
                    break;
                case enumDirection.West:
                    roverFIELD.RoverPosition.Direction = enumDirection.North;
                    break;
            }
        }
    }
}