﻿using System;

namespace Rover.TechTest
{
    class Program
    {
        static void Main(string[] args)
        {
            var roverRobot = new Rover();

            Console.WriteLine("Instructions:");
            Console.WriteLine("   Rotate Left : L");
            Console.WriteLine("   Rotate Right: R");
            Console.WriteLine("   Forward: F:");

            ConsoleKeyInfo keyInstruction;

            do
            {
                keyInstruction = Console.ReadKey(true);

                roverRobot.ProcessInstruction(keyInstruction);

                Console.WriteLine(roverRobot.ToString());

            } while (keyInstruction.Key != ConsoleKey.Escape);
        }
    }
}