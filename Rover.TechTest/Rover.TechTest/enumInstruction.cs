﻿namespace Rover.TechTest
{
    public enum enumInstruction
    {
        Left = 1,
        Right = 2,
        Forward = 3
    }
}