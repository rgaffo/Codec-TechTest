﻿namespace Rover.TechTest
{
    public class MoveForward : IInstruction
    {
        IRover roverFIELD;

        public MoveForward(IRover roverRobot)
        {
            roverFIELD = roverRobot;
        }

        public void Execute()
        {
            switch (roverFIELD.RoverPosition.Direction)
            {
                case enumDirection.North:
                    roverFIELD.RoverPosition.Row--;
                    break;
                case enumDirection.West:
                    roverFIELD.RoverPosition.Column--;
                    break;
                case enumDirection.South:
                    roverFIELD.RoverPosition.Row++;
                    break;
                case enumDirection.East:
                    roverFIELD.RoverPosition.Column++;
                    break;
            }
        }
    }
}