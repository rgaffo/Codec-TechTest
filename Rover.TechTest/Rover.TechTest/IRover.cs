﻿using System;

namespace Rover.TechTest
{
    public interface IRover
    {
        Position RoverPosition { get; set; }

        IInstruction RotateLeftCommand { get; set; }
        IInstruction RotateRightCommand { get; set; }
        IInstruction MoveForwardCommand { get; set; }

        void ProcessInstruction(ConsoleKeyInfo keyInstruction);
        string ToString();
    }
}