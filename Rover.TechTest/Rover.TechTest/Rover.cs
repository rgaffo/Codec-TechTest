﻿using System;

namespace Rover.TechTest
{
    public class Rover : IRover
    {
        public Position RoverPosition { get; set; }
        public IInstruction RotateLeftCommand { get; set; }
        public IInstruction RotateRightCommand { get; set; }
        public IInstruction MoveForwardCommand { get; set; }

        public Rover()
        {
            RoverPosition = new Position(1, 1, enumDirection.North);

            RotateLeftCommand = new RotateLeft(this);
            RotateRightCommand = new RotateRight(this);
            MoveForwardCommand = new MoveForward(this);
        }

        public void ProcessInstruction(ConsoleKeyInfo keyInstruction)
        {
            switch (keyInstruction.Key)
            {
                case ConsoleKey.F:
                    MoveForwardCommand.Execute();
                    break;
                case ConsoleKey.L:
                    RotateLeftCommand.Execute();
                    break;
                case ConsoleKey.R:
                    RotateRightCommand.Execute();
                    break;
            }
        }

        public override string ToString()
        {
            //return string.Format("{0},{1} - {2}", RoverPosition.Row, RoverPosition.Column, RoverPosition.Direction);
            return RoverPosition.ToString();
        }
    }
}