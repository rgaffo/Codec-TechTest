﻿namespace Rover.TechTest
{
    public class Position
    {
        private byte RowFIELD;
        private byte ColumnFIELD;

        public byte Row
        {
            get
            {
                return RowFIELD;
            }

            set
            {
                if ((value >= 0) && (value <= 3))
                    RowFIELD = value;
            }
        }

        public byte Column
        {
            get
            {
                return ColumnFIELD;
            }

            set
            {
                if ((value >= 0) && (value <= 3))
                    ColumnFIELD = value;
            }
        }

        public enumDirection Direction { get; set; }

        public Position(byte row, byte column, enumDirection direction)
        {
            Row = row;
            Column = column;
            Direction = direction;
        }

        public override string ToString()
        {
            return string.Format("{0},{1} - {2}", Row, Column, Direction);
        }
    }
}