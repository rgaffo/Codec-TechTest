﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Rover.TechTest.Tests
{
    [TestClass]
    public class RotateLeftTest
    {
        enumDirection expected;
        enumDirection actual;
        Rover target;

        ConsoleKeyInfo keyInstruction;

        [TestInitialize]
        public void InitializeTests()
        {
            target = new Rover();
            keyInstruction = new ConsoleKeyInfo('L', ConsoleKey.L, true, false, false);
        }

        [TestCleanup]
        public void CleanupTests()
        {
            target = null;
        }

        [TestMethod]
        //Given the rover is facing North, when  the river rotates Left, Then the rover is facing West.
        public void RotateLeftThenWest()
        {
            expected = enumDirection.West;
            target.ProcessInstruction(keyInstruction);

            actual = target.RoverPosition.Direction;

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        //Given the rover is facing West, when  the river rotates Left, Then the rover is facing South.
        public void RotateLeftThenSouth()
        {
            expected = enumDirection.South;
            target.ProcessInstruction(keyInstruction);
            target.ProcessInstruction(keyInstruction);

            actual = target.RoverPosition.Direction;

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        //Given the rover is facing South, when  the river rotates Left, Then the rover is facing East.
        public void RotateLeftThenEast()
        {
            expected = enumDirection.East;
            target.ProcessInstruction(keyInstruction);
            target.ProcessInstruction(keyInstruction);
            target.ProcessInstruction(keyInstruction);

            actual = target.RoverPosition.Direction;

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        //Given the rover is facing East, when  the river rotates Left, Then the rover is facing North.
        public void RotateLeftThenNorth()
        {
            expected = enumDirection.North;
            target.ProcessInstruction(keyInstruction);
            target.ProcessInstruction(keyInstruction);
            target.ProcessInstruction(keyInstruction);
            target.ProcessInstruction(keyInstruction);

            actual = target.RoverPosition.Direction;

            Assert.AreEqual(expected, actual);
        }
    }
}