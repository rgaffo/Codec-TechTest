﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Rover.TechTest.Tests
{
    [TestClass]
    public class RotateRightTest
    {
        enumDirection expected;
        enumDirection actual;
        Rover target;

        ConsoleKeyInfo keyInstruction;

        [TestInitialize]
        public void InitializeTests()
        {
            target = new Rover();
            keyInstruction = new ConsoleKeyInfo('R', ConsoleKey.R, true, false, false);
        }

        [TestCleanup]
        public void CleanupTests()
        {
            target = null;
        }

        [TestMethod]
        //Given the rover is facing North, when the rover rotates right, Then the rover is facing East.
        public void RotateRightThenEast()
        {
            expected = enumDirection.East;
            target.ProcessInstruction(keyInstruction);

            actual = target.RoverPosition.Direction;

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        //Given the rover is facing East, when the rover rotates right, Then the rover is facing South.
        public void RotateRightThenSouth()
        {
            expected = enumDirection.South;
            target.ProcessInstruction(keyInstruction);
            target.ProcessInstruction(keyInstruction);

            actual = target.RoverPosition.Direction;

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        //Given the rover is facing South, when the rover rotates right, Then the rover is facing West.
        public void RotateRightThenWest()
        {
            expected = enumDirection.West;
            target.ProcessInstruction(keyInstruction);
            target.ProcessInstruction(keyInstruction);
            target.ProcessInstruction(keyInstruction);

            actual = target.RoverPosition.Direction;

            Assert.AreEqual(expected, actual);
        }

        [TestMethod]
        //Given the rover is facing West, when the rover rotates right, Then the rover is facing North.
        public void RotateRightThenNorth()
        {
            expected = enumDirection.North;
            target.ProcessInstruction(keyInstruction);
            target.ProcessInstruction(keyInstruction);
            target.ProcessInstruction(keyInstruction);
            target.ProcessInstruction(keyInstruction);

            actual = target.RoverPosition.Direction;

            Assert.AreEqual(expected, actual);
        }
    }
}