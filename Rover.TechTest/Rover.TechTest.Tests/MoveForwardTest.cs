﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Rover.TechTest.Tests
{
    [TestClass]
    public class MoveForwardTest
    {
        Position expected;
        Position actual;
        Rover target;

        ConsoleKeyInfo keyInstruction;

        [TestInitialize]
        public void InitializeTests()
        {
            target = new Rover();
            keyInstruction = new ConsoleKeyInfo('F', ConsoleKey.F, true, false, false);
        }

        [TestCleanup]
        public void CleanupTests()
        {
            target = null;
            expected = null;
        }

        [TestMethod]
        //Given the Is at position 1.1 and the rover is facing North, when the rover moves forward, the rover is in position 0,1.
        public void MoveForwardThenPosition01()
        {
            expected = new Position(0, 1, enumDirection.North);

            target.ProcessInstruction(keyInstruction);

            actual = target.RoverPosition;

            Assert.AreEqual(expected.ToString(), actual.ToString());
        }
    }
}